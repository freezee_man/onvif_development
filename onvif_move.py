from onvif import ONVIFCamera #library import
from time import sleep

print 'INIT'

mycam = ONVIFCamera('192.168.13.12', 80, 'admin', 'Supervisor', '/etc/onvif/wsdl/') #initialize connection
print 'Connected to ONVIF camera'
media = mycam.create_media_service()
media_profile = media.GetProfiles()[0]
token = media_profile._token

print 'Creating PTZ object'
ptz = mycam.create_ptz_service()

request = ptz.create_type('GetServiceCapabilities')
Service_Capabilities = ptz.GetServiceCapabilities(request)
# print 'PTZ service capabilities:'
# print Service_Capabilities
status = ptz.GetStatus({'ProfileToken':token})
# print 'PTZ status:'
# print status
print 'Pan position:', status.Position.PanTilt._x
print 'Tilt position:', status.Position.PanTilt._y
print 'Zoom position:', status.Position.Zoom._x

reqconmv = ptz.create_type('ContinuousMove')
reqconmv.ProfileToken = media_profile._token

requests = ptz.create_type('Stop')
requests.ProfileToken = media_profile._token


#stop function
def stop():
	requests.PanTilt = True
	requests.Zoom = True
	print 'Stop:'
	print
	ptz.Stop(requests)
	print 'Stopped'


	
print
stop()


def move1(pan, tilt, zm, easy_time, move_speed): #moving to the set point with zooming                                                  
        print 'Start moving'                                                
        token = media_profile._token   #get ptz token                                     
        status = ptz.GetStatus({'ProfileToken':token})
        reqconmv.Velocity.PanTilt._x = move_speed
        reqconmv.Velocity.PanTilt._y = move_speed
        reqconmv.Velocity.Zoom._x = move_speed

        #print Pan, Tilt status
        print
        print 'Pan:', pan
        print 'Pan position:', status.Position.PanTilt._x
        print
        print 'Tilt:', tilt
        print 'Tilt position:', status.Position.PanTilt._y
        print
        print 'zm:', zm
        print 'Zoom position', status.Position.Zoom._x        
        #pan must be correct for zoom valuation                                                    
                                                                           
        while (abs(pan - status.Position.PanTilt._x) > 0.05) and (abs(tilt - status.Position.PanTilt._y) > 0.05) and (abs(zm - status.Position.Zoom._x) > 0.05): 
        #cycle works while status is less or more than the desired value (more than 0.05) 
            if pan > status.Position.PanTilt._x: #pan changing                           
                reqconmv.Velocity.PanTilt._x = 0.5                    
                print 'pan +'
            else:                                                           
                reqconmv.Velocity.PanTilt._x = -0.5
                print 'pan -'
            if tilt > status.Position.PanTilt._y: #tilt changing              
                reqconmv.Velocity.PanTilt._y = 0.5
                print 'tilt +'
            else:
                reqconmv.Velocity.PanTilt._y = -0.5 
                print 'tilt -'
            if (zm > status.Position.Zoom._x): #zoom changing
                	reqconmv.Velocity.Zoom._x = 0.05
                	print 'Zoom +'
            else:
            		reqconmv.Velocity.Zoom._x = -0.05
            		print 'Zoom -'
            		
            if (abs(pan - status.Position.PanTilt._x) < easy_time):
            		requestc.Velocity.PanTilt._x = 0
            		break

            if  (abs(pan - status.Position.PanTilt._y) < easy_time):
            		reqconmv.Velocity.PanTilt._y = 0
            		break

            if (abs(zm - status.Position.Zoom._x) < easy_time):
            		reqconmv.Velocity.Zoom._x = 0
            		break
            		
            ptz.ContinuousMove(reqconmv)                               
            status = ptz.GetStatus({'ProfileToken':token})
            status = ptz.GetStatus({'ProfileToken':token})
            #print status position
            print 'Pan position:', status.Position.PanTilt._x
            print 'Tilt position:', status.Position.PanTilt._y
            print 'Zoom position', status.Position.Zoom._x
            print '-------------------------------------------'
        stop()


#going home function
def goHome():
	print 'The camera moves home'
	request1 = ptz.create_type('GotoHomePosition')
	request1.ProfileToken = media_profile._token
	ptz.GotoHomePosition(request1)


goHome()
sleep(10)
move1(1, 1, 0.7, 0.3, 1)